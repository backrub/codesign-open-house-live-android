package com.jarvis.openhouselive.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jarvis.openhouselive.R;
import com.jarvis.openhouselive.model.common.Listing;
import com.jarvis.openhouselive.ui.custom.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class ListingsAdapter extends RecyclerView.Adapter<ListingsAdapter.ListingsHolder>{

    private ListingCallback listingCallback;
    private Context context;
    private ArrayList<Listing> listings;

    public ListingsAdapter(Context context, ListingCallback listingCallback) {
        listings = new ArrayList<>();
        this.context = context;
        this.listingCallback = listingCallback;
    }

    static class ListingsHolder extends RecyclerView.ViewHolder {
        CustomTextView name;
        CustomTextView area;
        CustomTextView price;
        CustomTextView locality;
        CustomTextView type;
        ImageButton button;
        ImageView imageView, share;
        LinearLayout detailsLayout;

        public ListingsHolder(View view) {
            super(view);
            area = (CustomTextView) view.findViewById(R.id.row_listing_area);
            name = (CustomTextView) view.findViewById(R.id.row_listing_name);
            price = (CustomTextView) view.findViewById(R.id.row_listing_price);
            locality = (CustomTextView) view.findViewById(R.id.row_listing_locality);
            type = (CustomTextView) view.findViewById(R.id.row_listing_type);
            imageView = (ImageView) view.findViewById(R.id.row_listing_image);
            share = (ImageView) view.findViewById(R.id.row_listing_share);
            button = (ImageButton) view.findViewById(R.id.row_listing_fab);
            detailsLayout = (LinearLayout) view.findViewById(R.id.row_listing_details);
        }
    }

    @Override
    public ListingsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_listing, parent, false);
        return new ListingsHolder(view);
    }

    @Override
    public void onBindViewHolder(ListingsHolder holder, final int position) {
        holder.locality.setText(listings.get(position).getLocality());
        holder.name.setText(listings.get(position).getName());
        holder.price.setText("\u20B9" + " " + listings.get(position).getPrice());
        holder.type.setText(listings.get(position).getType());
        holder.area.setText(listings.get(position).getArea());

        final Listing listing = listings.get(position);

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listingCallback != null) {
                    listingCallback.onShareRequested(listing);
                }
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listingCallback != null) {
                    listingCallback.onPanoramaRequested(listing);
                }
            }
        });

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listingCallback != null) {
                    listingCallback.onCardboardRequested(listing);
                }
            }
        });

        holder.detailsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listingCallback != null) {
                    listingCallback.onDetailsRequested(listing);
                }
            }
        });

        Picasso.with(context).load(listings.get(position).getImageUrl()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return listings.size();
    }

    @SuppressWarnings("unused")
    public void appendItems(ArrayList<Listing> depositDetailItems) {
        listings.addAll(depositDetailItems);
        notifyDataSetChanged();
    }

    @SuppressWarnings("unused")
    public void clear() {
        listings.clear();
        notifyDataSetChanged();
    }

    public interface ListingCallback {
        void onCardboardRequested(Listing listing);
        void onDetailsRequested(Listing listing);
        void onPanoramaRequested(Listing listing);
        void onShareRequested(Listing listing);
    }
}