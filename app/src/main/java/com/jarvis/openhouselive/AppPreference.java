package com.jarvis.openhouselive;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class AppPreference {

    private static final String KEY_FIRST_TIME = "key_first_time";
    private static final String KEY_SPLASH_DONE = "key_splash_done";

    private static AppPreference sInstance = new AppPreference();

    private SharedPreferences mSharedPreferences;

    private AppPreference() {
        mSharedPreferences = AppManager.getInstance().getContext().getSharedPreferences("app_preference", Context.MODE_PRIVATE);
    }

    public static synchronized AppPreference getInstance() {
        return sInstance;
    }

    public boolean isWalkThroughDone() {
        return getBoolean(KEY_SPLASH_DONE, false);
    }

    public void setWalkThroughDone(boolean value) {
        setBoolean(KEY_SPLASH_DONE, value);
    }

    /**
     * Core Methods
     * */

    protected synchronized boolean getBoolean(String key, boolean defaultValue) {
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    protected synchronized String getString(String key) {
        return mSharedPreferences.getString(key, "");
    }

    protected synchronized void setBoolean(String key, boolean value) {
        mSharedPreferences.edit().putBoolean(key, value).apply();
    }

    protected synchronized void setString(String key, String value) {
        mSharedPreferences.edit().putString(key, value).apply();
    }

    public synchronized void clearPreference() {
        mSharedPreferences.edit().clear().apply();
    }
}
