package com.jarvis.openhouselive.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.jarvis.openhouselive.R;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class CustomButton extends Button {

    public CustomButton(Context context) {
        super(context);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomButton, 0, 0);
        try {
            if (a.hasValue(R.styleable.CustomButton_font)) {
                setCustomFont(context, a.getString(R.styleable.CustomButton_font));
            }
        } finally {
            a.recycle();
        }
    }

    public CustomButton(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
        TypedArray a = context.getTheme().obtainStyledAttributes(attr, R.styleable.CustomButton, 0, 0);
        try {
            if (a.hasValue(R.styleable.CustomButton_font)) {
                setCustomFont(context, a.getString(R.styleable.CustomButton_font));
            }
        } finally {
            a.recycle();
        }
    }

    private void setCustomFont(Context context, String fontName) {
        try {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontName);
            setTypeface(typeface);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

}
