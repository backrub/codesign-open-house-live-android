package com.jarvis.openhouselive.rest;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public interface Requester {
    void failure();
    void success(Object response);
}
