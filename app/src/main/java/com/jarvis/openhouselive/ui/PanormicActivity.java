package com.jarvis.openhouselive.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jarvis.openhouselive.AppManager;
import com.jarvis.openhouselive.R;
import com.jarvis.openhouselive.model.common.Listing;
import com.jarvis.openhouselive.model.common.Marker;
import com.jarvis.openhouselive.ui.custom.OhHotspot;
import com.jarvis.openhouselive.util.Constants;
import com.panoramagl.PLIImage;
import com.panoramagl.PLIPanorama;
import com.panoramagl.PLITexture;
import com.panoramagl.PLImage;
import com.panoramagl.PLSpherical2Panorama;
import com.panoramagl.PLTexture;
import com.panoramagl.PLView;
import com.panoramagl.downloaders.PLFileDownloaderListener;
import com.panoramagl.downloaders.PLHTTPFileDownloader;
import com.panoramagl.enumerations.PLTextureColorFormat;
import com.panoramagl.hotspots.PLHotspot;
import com.panoramagl.hotspots.PLIHotspot;
import com.panoramagl.loaders.PLJSONLoader;
import com.panoramagl.transitions.PLTransitionBlend;
import com.panoramagl.utils.PLUtils;

import java.util.Timer;

public class PanormicActivity extends PLView {

    private PLSpherical2Panorama panorama;
    private static int identifier = 1000;

    Listing listing = null;
    Marker marker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Bundle bundle = getIntent().getExtras();
            listing = AppManager.getObjectFromJson(getIntent().getExtras().getString(Constants.EXTRA_KEY_LISTING), Listing.class);
            if(bundle.containsKey(Constants.EXTRA_KEY_MARKER)) {
                marker = AppManager.getObjectFromJson(getIntent().getExtras().getString(Constants.EXTRA_KEY_MARKER), Marker.class);
            } else {
                for(Marker mrk : listing.getMarkers()) {
                    if(mrk.getX().equalsIgnoreCase("0") && mrk.getY().equalsIgnoreCase("0")) {
                        marker = listing.getMarkers().get(0);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, "Invalid Data", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        //Load panorama
//        addMarker(marker);
//        panorama = new PLSpherical2Panorama();
//        panorama.getCamera().lookAt(30.0f, 90.0f);
//        panorama.setImage(new PLImage(PLUtils.getBitmap(this, R.raw.room), false));
//        this.setPanorama(panorama);
    }

    /**
     * This event is fired when root content view is created
     *
     * @param contentView current root content view
     * @return root content view that Activity will use
     */
    @Override
    protected View onContentViewCreated(View contentView) {
        ViewGroup mainView = (ViewGroup) this.getLayoutInflater().inflate(R.layout.activity_panormic, null);
        mainView.addView(contentView, 0);
        return super.onContentViewCreated(mainView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        addMarker(marker);
    }

    private void addMarker(Marker marker) {
        PLIPanorama panorama = null;
        this.setLocked(true);
        panorama = new PLSpherical2Panorama();
        ((PLSpherical2Panorama)panorama).setImage(new PLImage(PLUtils.getBitmap(this, getResourceId(marker.getImageUrl())), false));
        panorama.getCamera().lookAt(0.0f, 170.0f);

        for (final Marker mrk : listing.getMarkers()) {
            if(marker.getChildIds().contains(mrk.getMarkerId())) {
                if (!mrk.getX().equalsIgnoreCase("0") || !mrk.getY().equalsIgnoreCase("0")) {
                    float x = Float.parseFloat(mrk.getX());
                    float y = Float.parseFloat(mrk.getY());
                    OhHotspot hotspot = new OhHotspot(identifier++, new PLImage(PLUtils.getBitmap(this, R.raw.hotspot), false), y, x, 0.05f, 0.05f);
                    hotspot.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(PanormicActivity.this, PanormicActivity.class);
                            intent.putExtras(getIntent().getExtras());
                            intent.putExtra(Constants.EXTRA_KEY_MARKER, AppManager.getJsonString(mrk));
                            startActivity(intent);
                            finish();
                        }
                    });
                    panorama.addHotspot(hotspot);
                }
            }
        }
        this.reset();
        this.startTransition(new PLTransitionBlend(1.0f), panorama);
    }

    private int getResourceId(String url) {
        if(url.contains("p1")) {
            return R.raw.p1;
        } else if(url.contains("p2")) {
            return R.raw.p2;
        } else if(url.contains("p3")) {
            return R.raw.p3;
        } else if(url.contains("p4")) {
            return R.raw.p4;
        } else if(url.contains("p5")) {
            return R.raw.p5;
        }
        return R.raw.p1;
    }

}
