package com.jarvis.openhouselive.ui.custom;

import android.content.DialogInterface;
import android.view.View;

import com.panoramagl.PLIImage;
import com.panoramagl.PLIRenderer;
import com.panoramagl.PLIScene;
import com.panoramagl.PLITexture;
import com.panoramagl.PLTexture;
import com.panoramagl.hotspots.PLHotspot;
import com.panoramagl.interpreters.PLCommandInterpreter;

/**
 * Created by vissu on 11/1/15.
 */
public class OhHotspot extends PLHotspot {

    private View.OnClickListener listener;

    public OhHotspot(long identifier, float atv, float ath) {
        super(identifier, atv, ath);
    }

    public OhHotspot(long identifier, float atv, float ath, float width, float height) {
        super(identifier, atv, ath, width, height);
    }

    public OhHotspot(long identifier, PLITexture texture, float atv, float ath) {
        super(identifier, texture, atv, ath);
    }

    public OhHotspot(long identifier, PLITexture texture, float atv, float ath, float width, float height) {
        super(identifier, texture, atv, ath, width, height);
    }

    public OhHotspot(long identifier, PLIImage image, float atv, float ath) {
        this(identifier, (PLITexture) (new PLTexture(image)), atv, ath);
    }

    public OhHotspot(long identifier, PLIImage image, float atv, float ath, float width, float height) {
        this(identifier, (PLITexture)(new PLTexture(image)), atv, ath, width, height);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean touchDown(Object sender) {
        if(super.touchDown(sender)) {
//            if(getOnClick() != null && getOnClick().length() > 0) {
                PLCommandInterpreter commandInterpreter = new PLCommandInterpreter();
                if(sender instanceof PLIScene) {
                    listener.onClick(null);
                    commandInterpreter.interpret(((PLIScene)sender).getInternalView(), getOnClick());
                } else if(sender instanceof PLIRenderer) {
                    listener.onClick(null);
                    commandInterpreter.interpret(((PLIRenderer)sender).getInternalView(), getOnClick());
                }
//            }
            return true;
        } else {
            return false;
        }
    }
}

