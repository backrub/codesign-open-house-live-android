package com.jarvis.openhouselive.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class LocalityResponse {

    @SerializedName("localities")
    private ArrayList<String> localities;

    public ArrayList<String> getLocalities() {
        return localities;
    }
}
