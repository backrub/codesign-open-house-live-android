package com.jarvis.openhouselive;

import android.content.Context;

import com.google.gson.Gson;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class AppManager {

    private static AppManager ourInstance = new AppManager();

    public static AppManager getInstance() {
        return ourInstance;
    }

    private Context context;

    private AppManager() {
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public static String getJsonString(Object object) {
        return new Gson().toJson(object);
    }

    public static <T> T getObjectFromJson(String jsonString, Class<T> className) {
        return new Gson().fromJson(jsonString, className);
    }
}
