package com.jarvis.openhouselive.rest;

import retrofit.RestAdapter;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class RestBase {

    protected RestAdapter getBaseAdapter(String baseUrl) {
        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(baseUrl)
                .build();
    }

    protected RestService getRestService() {
        return getBaseAdapter("http://jarviscd.5gbfree.com/").create(RestService.class);
    }

}
