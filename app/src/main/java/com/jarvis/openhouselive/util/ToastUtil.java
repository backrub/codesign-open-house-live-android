package com.jarvis.openhouselive.util;

import android.widget.Toast;

import com.jarvis.openhouselive.AppManager;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class ToastUtil {

    public static void showToastLong(String text) {
        Toast.makeText(AppManager.getInstance().getContext(), text, Toast.LENGTH_LONG).show();
    }

    public static void showToastShort(String text) {
        Toast.makeText(AppManager.getInstance().getContext(), text, Toast.LENGTH_SHORT).show();
    }
}
